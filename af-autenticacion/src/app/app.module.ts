import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

// Módulo de enrutamiento
import { RoutingAppModule } from './routing-app.module';

// Módulos para autenticación y acceso a firebase
import { AngularFirestoreModule } from "@angular/fire/compat/firestore";
import { AngularFireModule } from "@angular/fire/compat";
import { environment } from "../environments/environment";
import { AngularFireAuthModule } from "@angular/fire/compat/auth";

// Componentes de la aplicación
import { AppComponent } from './app.component';
import { DashboardComponent } from './componentes/dashboard/dashboard.component';
import { SignInComponent } from './componentes/sign-in/sign-in.component';
import { SignUpComponent } from './componentes/sign-up/sign-up.component';
import { ForgotPasswordComponent } from './componentes/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './componentes/verify-email/verify-email.component';

// Mi servicio de autenticación
import { AuthService } from "./shared/services/auth.service";

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SignInComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent
  ],
  imports: [
    BrowserModule,
    RoutingAppModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
