import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SignInComponent} from "./componentes/sign-in/sign-in.component";
import {SignUpComponent} from "./componentes/sign-up/sign-up.component";
import {DashboardComponent} from "./componentes/dashboard/dashboard.component";
import {ForgotPasswordComponent} from "./componentes/forgot-password/forgot-password.component";
import {VerifyEmailComponent} from "./componentes/verify-email/verify-email.component";
import {AuthGuard} from "./shared/guard/auth.guard";

const rutas: Routes = [
  { path: 'sign-in', component: SignInComponent },
  { path: 'register-user', component: SignUpComponent },
  { path: 'dashboard' , component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'verify-email-address', component: VerifyEmailComponent },
  { path: '', redirectTo: '/sign-in', pathMatch: 'full' },
  { path: '**', redirectTo: '/sign-in', pathMatch: 'full'}
]

@NgModule({
  imports: [
    RouterModule.forRoot(rutas)
  ],
  exports: [
    RouterModule
  ]
})
export class RoutingAppModule { }
