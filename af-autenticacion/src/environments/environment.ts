// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAHeJmq50_xUEWirLRqcXIAuk37vqwWzFU",
    authDomain: "torneo-futbol7.firebaseapp.com",
    databaseURL: "https://torneo-futbol7.firebaseio.com",
    projectId: "torneo-futbol7",
    storageBucket: "torneo-futbol7.appspot.com",
    messagingSenderId: "49090420078",
    appId: "1:49090420078:web:f8dc9f174b359bc693f57f",
    measurementId: "G-PQ4DM30REY"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
