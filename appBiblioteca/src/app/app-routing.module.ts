import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PanelControlComponent } from './component/panel-control/panel-control.component';
import { AuthGuard } from './guard/auth.guard';


const routes: Routes = [
  //Principal
  {
    path: '',
    redirectTo: '/sign-in',
    pathMatch: 'full'
  },
  {
    path: 'index',
    component: PanelControlComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'autentication',
    loadChildren: () => import('./component/autentication/autentication.module').then(a => a.AutenticationModule)
  },
  {
    path: 'libros',
    loadChildren: () => import('./component/libros/libros.module').then(m => m.LibrosModule)
  },
  {
    path: 'socios',
    loadChildren: () => import('./component/socios/socios.module').then(s => s.SociosModule)
  },
  {
    path: 'prestamos',
    loadChildren: () => import('./component/prestamos/prestamos.module').then(p => p.PrestamosModule)
  }


  /*{
    path: '',
    component: SignInComponent,
  },*/
  //Error vuelve al indice
  /*{
    path: '**',
    pathMatch: 'full',
    redirectTo: '/index',

  }*/

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
