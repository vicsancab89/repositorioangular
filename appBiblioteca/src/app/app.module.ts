import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {AngularFireModule} from "@angular/fire/compat"
import { environment } from 'src/environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PanelControlComponent } from './component/panel-control/panel-control.component';

import { LibrosModule } from './component/libros/libros.module';
import { SociosModule } from './component/socios/socios.module';
import { PrestamosModule } from './component/prestamos/prestamos.module';
import { SharedModule } from './component/shared/shared.module';
import { AutenticationModule } from './component/autentication/autentication.module';


@NgModule({
  declarations: [
    AppComponent,
    PanelControlComponent,
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    BrowserModule,
    AppRoutingModule,
    AutenticationModule,
    BrowserAnimationsModule,
    LibrosModule,
    SociosModule,
    PrestamosModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
