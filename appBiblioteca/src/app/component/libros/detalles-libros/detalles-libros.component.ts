import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LibrosService } from 'src/app/services/libros/libros.service';

@Component({
  selector: 'app-detalles-libros',
  templateUrl: './detalles-libros.component.html',
  styleUrls: ['./detalles-libros.component.css']
})
export class DetallesLibrosComponent implements OnInit {
  //Objeto libro
  public libro : any = {};
  //String id
  public id: any = '';

  constructor(
    private librosService: LibrosService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    //Captura de id por ruta
    this.id = this.activatedRoute.snapshot.params.id;
  }

  ngOnInit(): void {
    //Obtención del objeto libro por ruta
    this.librosService.getLibro(this.activatedRoute.snapshot.params.id).subscribe((libroData :any)=>{
      this.libro = libroData.payload.data();
    });
  }

  //Método borrar
  public deleteLibro(documentId: any) {
    this.librosService.deleteLibro(documentId)
  }

}
