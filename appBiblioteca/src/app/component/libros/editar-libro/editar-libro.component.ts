import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LibrosService } from 'src/app/services/libros/libros.service';

@Component({
  selector: 'app-editar-libro',
  templateUrl: './editar-libro.component.html',
  styleUrls: ['./editar-libro.component.css'],
})
export class EditarLibroComponent implements OnInit {
  //Objeto libro
  libro: any = {};
  //Formulario libro
  libroForm: any;
  //ID libro por string
  idLibro: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private librosService: LibrosService,
    private router: Router
  ) {}

  ngOnInit(): void {
    //Id libro por ruta
    this.idLibro = this.activatedRoute.snapshot.paramMap.get('id');
    console.log(this.idLibro);
    //Objeto libro
    this.librosService.getLibro(this.idLibro).subscribe((libroData: any) => {
      this.libro = libroData.payload.data();
      //Formulario del libro aparecen los campos rellenos
      this.libroForm = new FormGroup({
        titulo: new FormControl(this.libro.titulo, Validators.required),
        autor: new FormControl(this.libro.autor, Validators.required),
        editorial: new FormControl( this.libro.editorial, Validators.required),
        prestamo: new FormControl( this.libro.prestamo, Validators.required),
        isbn: new FormControl(this.libro.isbn, Validators.required),
        img: new FormControl(this.libro.img, Validators.required),
        descripcion: new FormControl(this.libro.descripcion, Validators.required)
      });
    });
  }
  //Método para editar libros
  editLibro() {
    if (this.libroForm.valid) {
      this.librosService.updateLibro(this.idLibro, this.libroForm.value);
    }
    //Te devuelve a la lista de libros
    this.router.navigate(["/libros"]);
  }
}
