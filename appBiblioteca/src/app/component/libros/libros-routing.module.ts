import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/guard/auth.guard';
import { EditarLibroComponent } from './editar-libro/editar-libro.component';
import { DetallesLibrosComponent } from './detalles-libros/detalles-libros.component';
import { ListaLibrosComponent } from './lista-libros/lista-libros.component';
import { NuevoLibroComponent } from './nuevo-libro/nuevo-libro.component';

const routes: Routes = [
  {
    path: '',
    component: ListaLibrosComponent,
    canActivate: [AuthGuard]
  },
  //Nuevo libro
  {
    path: 'nuevoLibro',
    component: NuevoLibroComponent,
    canActivate: [AuthGuard]
  },
  //Detalles libro
  {
    path: ':id',
    component: DetallesLibrosComponent,
    canActivate: [AuthGuard]
  },
  //Edita Libros
  {
    path: 'editLibro/:id',
    component: EditarLibroComponent,
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LibrosRoutingModule { }
