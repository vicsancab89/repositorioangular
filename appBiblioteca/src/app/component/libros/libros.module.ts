import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LibrosRoutingModule } from './libros-routing.module';
import { DetallesLibrosComponent } from './detalles-libros/detalles-libros.component';
import { ListaLibrosComponent } from './lista-libros/lista-libros.component';
import { NuevoLibroComponent } from './nuevo-libro/nuevo-libro.component';
import { SharedModule } from '../shared/shared.module';
import { LibrosPipe } from 'src/app/pipe/libros/libros.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditarLibroComponent } from './editar-libro/editar-libro.component';


@NgModule({
  declarations: [
    DetallesLibrosComponent,
    ListaLibrosComponent,
    NuevoLibroComponent,
    EditarLibroComponent,
    LibrosPipe
  ],
  imports: [
    CommonModule,
    LibrosRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class LibrosModule { }
