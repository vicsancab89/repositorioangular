import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {LibrosService} from '../../../services/libros/libros.service';


@Component({
  selector: 'app-lista-libros',
  templateUrl: './lista-libros.component.html',
  styleUrls: ['./lista-libros.component.css']
})
export class ListaLibrosComponent implements OnInit {

  //Array de libros
  public libros: any[] = [];
  //filtra los libros
  public filtraLibro = '';

  constructor(
    private librosService: LibrosService,
    private route: Router
  ) { }

  ngOnInit(): void {
    //Carga los libros
    this.librosService.AllLibros().subscribe((librosSnapshot) => {
      this.libros = [];
      librosSnapshot.forEach((librosData: any) =>{
        this.libros.push({
          id: librosData.payload.doc.id,
          data: librosData.payload.doc.data()
        })
      })
    })
  }
  //Método delete
  public deleteLibro(documentId: any) {
    this.librosService.deleteLibro(documentId)
  }
}
