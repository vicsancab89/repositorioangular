import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LibrosService } from 'src/app/services/libros/libros.service';

@Component({
  selector: 'app-nuevo-libro',
  templateUrl: './nuevo-libro.component.html',
  styleUrls: ['./nuevo-libro.component.css']
})
export class NuevoLibroComponent implements OnInit {
  constructor(private libroService: LibrosService, private router: Router) {
    //Constructor Formulario libro
    this.newLibroForm.setValue({
      titulo: '',
      autor: '',
      editorial: '',
      prestamo: Boolean (false),
      isbn: '',
      img: '',
      descripcion: '',
    });
  }
  ngOnInit(): void {

  }
  //Nuevo Formulario
  public newLibroForm = new FormGroup({
    titulo: new FormControl('', Validators.required),
    autor: new FormControl('', Validators.required),
    editorial: new FormControl(''),
    prestamo: new FormControl( false, Validators.required),
    isbn: new FormControl('', Validators.required),
    img: new FormControl('', Validators.required),
    descripcion: new FormControl('', Validators.required)
  });
  //Datos de los campos
  public newLibro(form: { titulo: any; autor: any; editorial: any; prestamo: any; isbn: any; img: any, descripcion: any}) {
    let data = {
      titulo: form.titulo,
      autor: form.autor,
      editorial: form.editorial,
      prestamo: form.prestamo,
      isbn: form.isbn,
      img: form.img,
      descripcion: form.descripcion
    }
    //Creaccion del libro en Firebasse
    this.libroService.crearLibro(data).then(() => {
      this.newLibroForm.setValue({
        titulo: '',
        autor: '',
        editorial: '',
        isbn: '',
        prestamo: Boolean(false),
        img: '',
        descripcion: ''
      });
    });
    //Ruta a lista de libros
    this.router.navigate(["/libros"]);
  }
}
