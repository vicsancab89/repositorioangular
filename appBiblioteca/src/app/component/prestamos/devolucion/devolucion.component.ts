import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LibrosService } from 'src/app/services/libros/libros.service';
import { PrestamosService } from 'src/app/services/prestamos/prestamos.service';
import { SociosService } from 'src/app/services/socios/socios.service';

@Component({
  selector: 'app-devolucion',
  templateUrl: './devolucion.component.html',
  styleUrls: ['./devolucion.component.css']
})
export class DevolucionComponent implements OnInit {
  //Objetos de prestamo, libro y socio
  prestamo: any = [];
  libro: any = {};
  socio: any = {};
  //formulario de prestamo formulario de libro lo usamos para cambiar su estado
  prestamoForm: any;
  libroForm: any;
  //id del Prestamo
  idPrestamo: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private prestamosService: PrestamosService,
    private librosService: LibrosService,
    private sociosService: SociosService,
    private router: Router
  ) {}

  ngOnInit(): void {
    //Obtención del id de prestamo
    this.idPrestamo = this.activatedRoute.snapshot.paramMap.get('id');
    //Obtención de prestamo
    this.prestamosService.getPrestamo(this.idPrestamo).subscribe((prestamoData: any) => {
      this.prestamo = prestamoData.payload.data();
      //Formulario de prestamo
      this.prestamoForm = new FormGroup({
        libroId: new FormControl(this.prestamo.libroId, Validators.required),
        socioId: new FormControl(this.prestamo.socioId, Validators.required),
        prestado: new FormControl( this.prestamo.prestado, Validators.required),
        devuelto: new FormControl( this.prestamo.devuelto, Validators.required),
      });
      //Obtención de libro
      this.librosService.getLibro(this.prestamo.libroId).subscribe((libroData :any)=>{
        this.libro = libroData.payload.data();
      });
      //Obtención de socio
      this.sociosService.getSocio(this.prestamo.socioId).subscribe((socioData :any)=>{
        this.socio = socioData.payload.data();
      });
    });
  }

  //Método de devolución aquí cambiamos prestamo a false
  devolucion() {
    if (this.prestamoForm.valid) {
      this.prestamosService.updatePrestamo(this.idPrestamo, this.prestamoForm.value);
    }
    //Formulario del libro
    this.libroForm = new FormGroup({
      titulo: new FormControl(this.libro.titulo, Validators.required),
      autor: new FormControl(this.libro.autor, Validators.required),
      editorial: new FormControl( this.libro.editorial, Validators.required),
      prestamo: new FormControl( Boolean (false), Validators.required),
      isbn: new FormControl(this.libro.isbn, Validators.required),
      img: new FormControl(this.libro.img, Validators.required),
      descripcion: new FormControl(this.libro.descripcion, Validators.required)
    });
    //Si es valido editamos el libro
    if (this.libroForm.valid) {
      this.librosService.updateLibro(this.prestamo.libroId, this.libroForm.value);
    }
    //Nos devuelve a prestamos
    this.router.navigate(["/prestamos"]);
  }
}
