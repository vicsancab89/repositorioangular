import { Component, OnInit } from '@angular/core';
import { LibrosService } from 'src/app/services/libros/libros.service';
import { PrestamosService } from 'src/app/services/prestamos/prestamos.service';
import { SociosService } from 'src/app/services/socios/socios.service';

@Component({
  selector: 'app-lista-prestamos',
  templateUrl: './lista-prestamos.component.html',
  styleUrls: ['./lista-prestamos.component.css']
})
export class ListaPrestamosComponent implements OnInit {
  //Lista de prestamos
  public prestamos: any[] = [];
  //Lista de socios
  public socios : any = [];
  //Lista de libros
  public libros : any = [];

  constructor(
    private prestamosService: PrestamosService,
    private sociosService: SociosService,
    private librosService : LibrosService

  ) { }

  ngOnInit(): void {
    //Carga los prestamos
    this.prestamosService.AllPrestamos().subscribe((prestamosSnapshot) => {
      this.prestamos = [];
      prestamosSnapshot.forEach((prestamosData: any) =>{
        this.prestamos.push({
          id: prestamosData.payload.doc.id,
          data: prestamosData.payload.doc.data(),
        })
      })
    })
    //Carga los libros
    this.librosService.AllLibros().subscribe((librosSnapshot) => {
      this.libros = [];
      librosSnapshot.forEach((librosData: any) =>{
        this.libros.push({
          id: librosData.payload.doc.id,
          data: librosData.payload.doc.data()
        })
      })
    })
    //Carga los socios
    this.sociosService.AllSocios().subscribe((sociosSnapshot) => {
      this.socios = [];
      sociosSnapshot.forEach((sociosData: any) =>{
        this.socios.push({
          id: sociosData.payload.doc.id,
          data: sociosData.payload.doc.data()
        })
      })
    })
  }
  //Borra prestamo
  public deletePrestamo(documentId: any) {
    this.prestamosService.deletePrestamo(documentId)
  }

  //Comparador de primary key y foreign key
  compareId(pk:String, fk:String): Boolean{
    return Boolean (String (pk.trim()) == String (fk.trim()));
  }
}
