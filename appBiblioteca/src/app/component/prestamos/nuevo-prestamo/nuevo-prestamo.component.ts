
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LibrosService } from 'src/app/services/libros/libros.service';
import { PrestamosService } from 'src/app/services/prestamos/prestamos.service';
import { SociosService } from 'src/app/services/socios/socios.service';

@Component({
  selector: 'app-nuevo-prestamo',
  templateUrl: './nuevo-prestamo.component.html',
  styleUrls: ['./nuevo-prestamo.component.css']
})
export class NuevoPrestamoComponent implements OnInit {
  //Socios y libros
  public socios : any[] = [];
  public libros : any[] = [];
  //Objeto libro
  public libro : any = {};
  //Id libro
  public idLibro : any = '';
  //Formulario libro
  public libroForm: any;
  public listresult: any[] = [];
  constructor(
    private prestamosService: PrestamosService,
    private librosService: LibrosService,
    private sociosService: SociosService,
    private router: Router) {
    //Constructor nuevo prestamo
    this.newPrestamoForm.setValue({
      socioId: '',
      libroId: '',
      prestado: '',
      devuelto: null,
    });
  }
  ngOnInit(): void {
    //Carga de libros
    this.librosService.AllLibros().subscribe((librosSnapshot) => {
      this.libros = [];
      librosSnapshot.forEach((librosData: any) =>{
        if(!librosData.payload.doc.data().prestamo){
        this.libros.push({
          id: librosData.payload.doc.id,
          data: librosData.payload.doc.data()
        })}
      })
    })
    //Carga de socios
    this.sociosService.AllSocios().subscribe((sociosSnapshot) => {
      this.socios = [];
      sociosSnapshot.forEach((sociosData: any) =>{
        this.socios.push({
          id: sociosData.payload.doc.id,
          data: sociosData.payload.doc.data()
        })
      })
    })
  }
  //Nuevo formulario prestamo
  public newPrestamoForm = new FormGroup({
    socioId: new FormControl('', Validators.required),
    libroId: new FormControl('', Validators.required),
    prestado: new FormControl('', Validators.required ),
    devuelto: new FormControl(''),
  });
  //Carga de datos de prestamo
  public newPrestamo(form: { socioId: any; libroId: any; prestado: any; devuelto: any;}) {
    let data = {
      socioId: form.socioId,
      libroId: form.libroId,
      prestado: form.prestado,
      devuelto: form.devuelto,
    }
    //Cambio del estado del libro
    this.idLibro = form.libroId;
    this.librosService.getLibro(this.idLibro).subscribe((libroData: any) => {
      this.libro = libroData.payload.data();
      this.libroForm = new FormGroup({
        titulo: new FormControl(this.libro.titulo, Validators.required),
        autor: new FormControl(this.libro.autor, Validators.required),
        editorial: new FormControl( this.libro.editorial, Validators.required),
        prestamo: new FormControl(Boolean (true), Validators.required),
        isbn: new FormControl(this.libro.isbn, Validators.required),
        img: new FormControl(this.libro.img, Validators.required),
        descripcion: new FormControl(this.libro.descripcion, Validators.required)
      });
      //Cambio del estado del libro en firebase
      if (this.libroForm.valid) {
        this.librosService.updateLibro(this.idLibro, this.libroForm.value);
      }
    });
    this.prestamosService.crearPrestamo(data).then(() => {
      this.newPrestamoForm.setValue({
        socioId: '',
        libroId: '',
        prestado: '',
        devuelto: '',
      });
    });
    this.router.navigate(["/prestamos"]);
  }
  //Metodo para que aparezcan solo los libros no prestado
  public filtraLibro(): any {

    this.libros.forEach(libro => {
      if(!libro.data.prestado){
        this.listresult.push(libro);
      }
    });
    return this.listresult;
  }
}
