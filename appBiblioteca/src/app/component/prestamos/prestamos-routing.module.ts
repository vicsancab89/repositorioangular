import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/guard/auth.guard';
import { DevolucionComponent } from './devolucion/devolucion.component';
import { ListaPrestamosComponent } from './lista-prestamos/lista-prestamos.component';
import { NuevoPrestamoComponent } from './nuevo-prestamo/nuevo-prestamo.component';

const routes: Routes = [
  //Lista prestamos
  {
    path: '',
    component: ListaPrestamosComponent,
    canActivate: [AuthGuard]
  },
  //Nuevo prestamo
  {
    path: 'nuevoPrestamo',
    component: NuevoPrestamoComponent,
    canActivate: [AuthGuard]
  },
  //Devolución
  {
    path: 'devolucion/:id',
    component: DevolucionComponent,
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrestamosRoutingModule { }
