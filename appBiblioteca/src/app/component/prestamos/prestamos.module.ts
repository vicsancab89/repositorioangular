import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrestamosRoutingModule } from './prestamos-routing.module';
import { DevolucionComponent } from './devolucion/devolucion.component';
import { NuevoPrestamoComponent } from './nuevo-prestamo/nuevo-prestamo.component';
import { ListaPrestamosComponent } from './lista-prestamos/lista-prestamos.component';
import { SharedModule } from '../shared/shared.module';
import { PrestamosPipe } from 'src/app/pipe/prestamos/prestamos.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    DevolucionComponent,
    NuevoPrestamoComponent,
    ListaPrestamosComponent,
    PrestamosPipe
  ],
  imports: [
    CommonModule,
    PrestamosRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PrestamosModule { }
