import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesSociosComponent } from './detalles-socios.component';

describe('DetallesSociosComponent', () => {
  let component: DetallesSociosComponent;
  let fixture: ComponentFixture<DetallesSociosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetallesSociosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesSociosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
