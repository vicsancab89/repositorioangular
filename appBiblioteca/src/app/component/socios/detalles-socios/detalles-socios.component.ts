import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SociosService } from 'src/app/services/socios/socios.service';

@Component({
  selector: 'app-detalles-socios',
  templateUrl: './detalles-socios.component.html',
  styleUrls: ['./detalles-socios.component.css']
})
export class DetallesSociosComponent implements OnInit {
  //Objeto socio
  socio: any = {};
  //String id
  id : any = '';
  constructor(
    private sociosService: SociosService,
    private activatedRoute: ActivatedRoute
  ) {
    //id por ruta
    this.id = this.activatedRoute.snapshot.params.id;
   }

  ngOnInit(): void {
    //Obtención Objeto de socio por ruta
    this.sociosService.getSocio(this.activatedRoute.snapshot.params.id).subscribe((socioData :any)=>{
      this.socio = socioData.payload.data();
    });
  }
  //Método Delete
  public deleteSocio(documentId: any) {
    this.sociosService.deleteSocio(documentId)
  }

}
