import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SociosService } from 'src/app/services/socios/socios.service';

@Component({
  selector: 'app-editar-socio',
  templateUrl: './editar-socio.component.html',
  styleUrls: ['./editar-socio.component.css']
})
export class EditarSocioComponent implements OnInit {
  //Objeto socio
  socio: any = {};
  //Formulario socio
  socioForm: any;
  //id socio
  idSocio: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private sociosService: SociosService,
    private router: Router
  ) {}

  ngOnInit(): void {
    //id por ruta
    this.idSocio = this.activatedRoute.snapshot.paramMap.get('id');
    //Objeto socio
    this.sociosService.getSocio(this.idSocio).subscribe((libroData: any) => {
      this.socio = libroData.payload.data();
      //Formulario de socio con los campos rellenos
      this.socioForm = new FormGroup({
        nombre: new FormControl(this.socio.nombre, Validators.required),
        apellido: new FormControl(this.socio.apellido, Validators.required),
        correo: new FormControl( this.socio.correo, Validators.required),
        telefono: new FormControl(this.socio.telefono, Validators.required),
        direccion: new FormControl(this.socio.direccion, Validators.required),
        img: new FormControl(this.socio.img, Validators.required),
      });
    });
  }

  //Método editar socio
  editSocio() {
    if (this.socioForm.valid) {
      this.sociosService.updateSocios(this.idSocio, this.socioForm.value);
    }
    this.router.navigate(["/socios"]);
  }}
