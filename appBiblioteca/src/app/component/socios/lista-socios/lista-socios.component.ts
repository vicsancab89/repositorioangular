import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SociosService } from 'src/app/services/socios/socios.service';

@Component({
  selector: 'app-lista-socios',
  templateUrl: './lista-socios.component.html',
  styleUrls: ['./lista-socios.component.css']
})
export class ListaSociosComponent implements OnInit {
  //Array de socios
  public socios: any[] = [];
  //Filtrado de socios
  public filtraSocio = '';
  constructor(
    private sociosService: SociosService,
    private route: Router
  ) { }

  ngOnInit(): void {
    //Carga de los socios
    this.sociosService.AllSocios().subscribe((sociosSnapshot) => {
      this.socios = [];
      sociosSnapshot.forEach((sociosData: any) =>{
        this.socios.push({
          id: sociosData.payload.doc.id,
          data: sociosData.payload.doc.data()
        })
      })
    })
  }
}
