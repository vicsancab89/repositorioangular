import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoSocioComponent } from './nuevo-socio.component';

describe('NuevoSocioComponent', () => {
  let component: NuevoSocioComponent;
  let fixture: ComponentFixture<NuevoSocioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NuevoSocioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoSocioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
