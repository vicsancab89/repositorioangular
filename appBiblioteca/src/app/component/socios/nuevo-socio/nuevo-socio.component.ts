import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SociosService } from '../../../services/socios/socios.service';

@Component({
  selector: 'app-nuevo-socio',
  templateUrl: './nuevo-socio.component.html',
  styleUrls: ['./nuevo-socio.component.css']
})
export class NuevoSocioComponent implements OnInit {
  //Lista de socios
  socios : any[] = [];
  constructor(private sociosService: SociosService, private router: Router) {
    //Nuevo formulario socio
    this.newSocioForm.setValue({
      nombre: '',
      apellido: '',
      correo: '',
      telefono: '',
      direccion: '',
      img: '',
    });
  }
  ngOnInit() {
    //Carga de socios
    this.sociosService.AllSocios().subscribe((sociosSnapshot) => {
      this.socios = [];
      sociosSnapshot.forEach((sociosData: any) =>{
        this.socios.push({
          id: sociosData.payload.doc.id,
          data: sociosData.payload.doc.data()

        })
      })
    })
  }
  //Nuevo formulario de socio
  public newSocioForm = new FormGroup({
    nombre: new FormControl('', Validators.required),
    apellido: new FormControl('', Validators.required),
    correo: new FormControl( '', Validators.required),
    telefono: new FormControl('', Validators.required),
    direccion: new FormControl('', Validators.required),
    img: new FormControl('', Validators.required)
  });
  //Carga del nuevo socio
  public newSocio(form: { nombre: any; apellido: any; correo: any; telefono: any; direccion: any; img: any}) {
    let data = {
      nombre: form.nombre,
      apellido: form.apellido,
      correo: form.correo,
      telefono: form.telefono,
      direccion: form.direccion,
      img: form.img,
    }
    this.sociosService.crearSocio(data).then(() => {
      this.newSocioForm.setValue({
        nombre: '',
        apellido: '',
        correo: '',
        telefono: '',
        direccion: '',
        img: '',
      });
    });
    //Ruta a lista de socios
    this.router.navigate(["/socios"]);
  }
}
