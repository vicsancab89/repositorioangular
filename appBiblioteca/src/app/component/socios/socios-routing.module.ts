import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/guard/auth.guard';
import { DetallesSociosComponent } from './detalles-socios/detalles-socios.component';
import { EditarSocioComponent } from './editar-socio/editar-socio.component';
import { ListaSociosComponent } from './lista-socios/lista-socios.component';
import { NuevoSocioComponent } from './nuevo-socio/nuevo-socio.component';

const routes: Routes = [
   //Lista socios
   {
    path: '',
    component: ListaSociosComponent,
    canActivate: [AuthGuard]
  },
  //Nuevo socio
  {
    path: 'nuevoSocio',
    component: NuevoSocioComponent,
    canActivate: [AuthGuard]
  },
  //Detalles socio
  {
    path: ':id',
    component: DetallesSociosComponent,
    canActivate: [AuthGuard]
  },
  //Edita socio
  {
    path: 'editSocio/:id',
    component: EditarSocioComponent,
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SociosRoutingModule { }
