import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SociosRoutingModule } from './socios-routing.module';
import { DetallesSociosComponent } from './detalles-socios/detalles-socios.component';
import { EditarSocioComponent } from './editar-socio/editar-socio.component';
import { ListaSociosComponent } from './lista-socios/lista-socios.component';
import { NuevoSocioComponent } from './nuevo-socio/nuevo-socio.component';
import { SociosPipe } from 'src/app/pipe/socios/socios.pipe';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    DetallesSociosComponent,
    EditarSocioComponent,
    ListaSociosComponent,
    NuevoSocioComponent,
    SociosPipe
  ],
  imports: [
    CommonModule,
    SociosRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class SociosModule { }
