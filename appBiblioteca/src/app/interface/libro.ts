export interface Libro{
  id: string,
  data: {titulo: string, autor: string, editorial: string, isbn: string, img: string}
}
