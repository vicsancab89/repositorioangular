export interface Socio {
  id: string,
  data: {nombre: string, apellidos: string, carnet: number, direccion: string, correo: string, telefono: string, img: string}
}
