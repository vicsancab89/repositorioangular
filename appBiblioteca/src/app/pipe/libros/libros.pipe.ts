import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtradoLibro'
})
export class LibrosPipe implements PipeTransform {

  transform(value: any , arg: any): any {
    const result = [];
    for(const libro of value){
    if((libro.data.titulo.toLowerCase().indexOf(arg) > -1) || (libro.data.titulo.indexOf(arg) > -1)
    || (libro.data.autor.toLowerCase().indexOf(arg) > -1) || (libro.data.autor.indexOf(arg) > -1)
    || (libro.data.editorial.toLowerCase().indexOf(arg) > -1) || (libro.data.editorial.indexOf(arg) > -1)
    || (libro.data.isbn.toLowerCase().indexOf(arg) > -1) || (libro.data.isbn.indexOf(arg) > -1)
    ){
      result.push(libro);
   };
  };
    return result;
  }


}
