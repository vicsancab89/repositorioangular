import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtradoPrestamos'
})
export class PrestamosPipe implements PipeTransform {

  transform(value: any , arg: any): any {
    const result = [];
    for(const objet of value){
    if((objet.data.nombre.toLowerCase().indexOf(arg) > -1) || (objet.data.nombre.indexOf(arg) > -1)
    || (objet.data.apellido.toLowerCase().indexOf(arg) > -1) || (objet.data.apellido.indexOf(arg) > -1)
    || (objet.data.titulo.toLowerCase().indexOf(arg) > -1) || (objet.data.titulo.indexOf(arg) > -1)
    || (objet.data.socioId.toLowerCase().indexOf(arg) > -1) || (objet.data.socioId.indexOf(arg) > -1)
    ){
      result.push(objet);
   };
  };
    return result;
  }
}
