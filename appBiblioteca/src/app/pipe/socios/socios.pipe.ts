import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtradoSocio'
})
export class SociosPipe implements PipeTransform {
  transform(value: any , arg: any): any {
    const result = [];
    for(const socio of value){
    if((socio.data.nombre.toLowerCase().indexOf(arg) > -1) || (socio.data.nombre.indexOf(arg) > -1)
    || (socio.data.apellido.toLowerCase().indexOf(arg) > -1) || (socio.data.apellido.indexOf(arg) > -1)
    || (socio.data.direccion.toLowerCase().indexOf(arg) > -1) || (socio.data.direccion.indexOf(arg) > -1)
    || (socio.data.correo.toLowerCase().indexOf(arg) > -1) || (socio.data.correo.indexOf(arg) > -1)
    || (socio.data.telefono.toLowerCase().indexOf(arg) > -1) || (socio.data.telefono.indexOf(arg) > -1)
    || (socio.id.toLowerCase().indexOf(arg) > -1) || (socio.id.indexOf(arg) > -1)
    ){
      result.push(socio);
   };
  };
    return result;
  }
}
