import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Libro } from 'src/app/interface/libro';
@Injectable({
  providedIn: 'root'
})
export class LibrosService {
  constructor(
    private firestore: AngularFirestore
  ) { }

  //Crea un nuevo libro.
  public crearLibro(data: {titulo: string, autor: string, editorial: string, isbn: string, prestamo: boolean, img: string, descripcion: string}){
    return this.firestore.collection('libro').add(data);
  }

  //Obtiene un libro
  public getLibro(id: string ){
    return this.firestore.collection('libro').doc(id).snapshotChanges();
  }

  //Obtiene todos los libros
  public AllLibros(){
    return this.firestore.collection('libro').snapshotChanges();
  }

  //Actualiza un libro
  public updateLibro(id: string, data:any){
    return this.firestore.collection('libro').doc(id).set(data);
  }

  //Borra un libro
  public deleteLibro(documentId: string){
    return this.firestore.collection('libro').doc(documentId).delete();
  }

}
