import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class PrestamosService {

  constructor(
    private firestore: AngularFirestore
  ) { }

  //Crea un nuevo prestamo.
  public crearPrestamo(data: { socioId: any, libroId: any, prestado: any, devuelto: any}){
    return this.firestore.collection('prestamo').add(data);
  }

  //Obtiene un prestamo
  public getPrestamo(id: string ){
    return this.firestore.collection('prestamo').doc(id).snapshotChanges();
  }

  //Obtiene todos los prestamos
  public AllPrestamos(){
    return this.firestore.collection('prestamo').snapshotChanges();
  }

  //Actualiza un prestamo
  public updatePrestamo(id: string, data:any){
    return this.firestore.collection('prestamo').doc(id).set(data);
  }

  //Borra un prestamo
  public deletePrestamo(documentId: string){
    return this.firestore.collection('prestamo').doc(documentId).delete();
  }
}
