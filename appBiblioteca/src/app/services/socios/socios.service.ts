import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Socio } from 'src/app/interface/socio';

@Injectable({
  providedIn: 'root'
})
export class SociosService {

  constructor( private firestore: AngularFirestore) { }
  //Crea un nuevo socio.
  public crearSocio(data: {nombre: string, apellido: string, direccion: string, correo: string, telefono: string, img: string}){
    return this.firestore.collection('socio').add(data);
  }

  //Obtiene un socio
  public getSocio(documentId: string){
    return this.firestore.collection('socio').doc(documentId).snapshotChanges();
  }

  //Obtiene todos los socios
  public AllSocios(){
    return this.firestore.collection('socio').snapshotChanges();
  }

  //Actualiza un socio
  public updateSocios(documentId: string, data:any){
    return this.firestore.collection('socio').doc(documentId).set(data);
  }
  //Borra un socio
  public deleteSocio(documentId: string){
    return this.firestore.collection('socio').doc(documentId).delete();
  }
}
