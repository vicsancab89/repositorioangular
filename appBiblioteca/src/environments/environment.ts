// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyD16tHo5MdgxOdnEV5XZD-1xMKC15gxMjY",
    authDomain: "michus-3f478.firebaseapp.com",
    projectId: "michus-3f478",
    storageBucket: "michus-3f478.appspot.com",
    messagingSenderId: "124743645291",
    appId: "1:124743645291:web:51959364bfe9a92443592e"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
