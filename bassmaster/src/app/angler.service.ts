import { Injectable } from '@angular/core';
import { Elite } from './mocks/angler-mock';

@Injectable({
  providedIn: 'root'
})
export class AnglerService {



  constructor() { }

  getAnglers(){
    return Elite;
  }
  getAngler(id: number){
    return Elite[id];
  }
  filterAngler(numGanados: number){
    if(numGanados == 0)
    return Elite.filter(angler => angler.ganados == numGanados);
    else
    return Elite.filter(angler => angler.ganados > 0);
  }
}
