export interface Angler {
  id: number;
  nombre: string;
  descripcion: string;
  torneos: number;
  ganados: number;
  foto: string;
}
