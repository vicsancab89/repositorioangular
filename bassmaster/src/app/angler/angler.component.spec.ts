import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnglerComponent } from './angler.component';

describe('AnglerComponent', () => {
  let component: AnglerComponent;
  let fixture: ComponentFixture<AnglerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnglerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnglerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
