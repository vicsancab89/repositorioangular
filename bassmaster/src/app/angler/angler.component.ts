import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Angler } from '../angler';
import { AnglerService } from '../angler.service';

@Component({
  selector: 'app-angler',
  templateUrl: './angler.component.html',
  styleUrls: ['./angler.component.css']
})
export class AnglerComponent implements OnInit {

  listAnglers: Angler[] = [];
  constructor( private anglerService: AnglerService, private route: Router) { }

  ngOnInit(): void {
    this.listAnglers = this.anglerService.getAnglers();
  }

  showAngler(id: number){
    this.route.navigate(['angler', id]);
  }
  selectAngler(ganados?: number){
    if(ganados == null)
      this.ngOnInit();
    else
    this.listAnglers = this.anglerService.filterAngler(ganados);
  }
}
