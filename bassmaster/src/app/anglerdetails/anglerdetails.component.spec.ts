import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnglerdetailsComponent } from './anglerdetails.component';

describe('AnglerdetailsComponent', () => {
  let component: AnglerdetailsComponent;
  let fixture: ComponentFixture<AnglerdetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnglerdetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnglerdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
