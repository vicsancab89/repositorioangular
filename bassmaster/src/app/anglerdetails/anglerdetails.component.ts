import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { AnglerService } from '../angler.service';

@Component({
  selector: 'app-anglerdetails',
  templateUrl: './anglerdetails.component.html',
  styleUrls: ['./anglerdetails.component.css']
})
export class AnglerdetailsComponent implements OnInit {
  angler: any = {}
  constructor(private anglersService: AnglerService, private activatedRoute : ActivatedRoute) { }

  ngOnInit(): void {
    this.angler = this.anglersService.getAngler(this.activatedRoute.snapshot.params.id)
  }


}
