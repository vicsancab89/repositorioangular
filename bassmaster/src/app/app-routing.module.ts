import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnglerComponent } from './angler/angler.component';
import { AnglerdetailsComponent } from './anglerdetails/anglerdetails.component';

const routes: Routes = [
  {
    path: 'angler',
    component: AnglerComponent
  },
  {
    path: 'angler/:id',
    component: AnglerdetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
