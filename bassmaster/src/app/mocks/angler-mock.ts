import { Angler } from "../angler";

export const Elite: Angler [] = [
  {
    id: 1,
    nombre: "Seth Feider",
    descripcion: "He known for sporting one of fishing's best cookie dusters, is a current Bassmaster Elite Angler sponsored by Rapala, VMC, Simms, Daiwa, and OutKast Tackle. Feider, a Bloomington, MN native, won the 2016 Toyota Angler of the Year Tournament on Lake Mille Lacs in northern Minnesota with smallmouth bass.",
    torneos: 89,
    ganados: 3,
    foto: "https://www.bassmaster.com/sites/default/files/persons/media%2C%20browser/feider_seth_1.jpg",
  },
  {
    id: 2,
    nombre: "Brandon Palaniuk",
    descripcion: "Won the B.A.S.S. Nation Championship in 2010. His Bassmaster Elite Series career began in 2011. Palaniuk has earned five Elite wins and qualified for nine Bassmaster Classics. He was the 2017 Bassmaster Angler of the Year.",
    torneos: 129,
    ganados: 6,
    foto: "https://www.bassmaster.com/sites/default/files/persons/media%2C%20browser/palanuik_brandon.jpg",
  },
  {
    id: 3,
    nombre: "David Fritts",
    descripcion: "One of the most successful veterans in all of bass fishing, David Fritts has been professionally fishing for over 20 years. Fritts is known as the crankbait master and he has used this bait almost exclusively to notch his numerous victories and top-10 finishes with both FLW and BASS tournament circuits.",
    torneos: 209,
    ganados: 5,
    foto: "https://www.bassmaster.com/sites/default/files/persons/media%2C%20browser/fritts_david.jpg",
  },
  {
    id: 4,
    nombre: "John Crews",
    descripcion: "In 2021, John Crews will compete in his 16th year on the Bassmaster Elite Series. Crews notched an Elite Series victory in 2008 at the California Delta. He is a 12-time Classic qualifier and the Owner/Operator of Missile Baits.",
    torneos: 	208,
    ganados: 1,
    foto: "https://www.bassmaster.com/sites/default/files/persons/media%2C%20browser/crews_john_1.jpg",
  },
  {
    id: 5,
    nombre: "Rick Clunn",
    descripcion: "A legend in the sport of bass fishing, is fishing his 16th year on the Bassmaster Elite Series in 2021. Clunn has four Bassmaster Classic titles (1976, 1977, 1984, 1990) and 16 Bassmaster tournament wins. Clunn has 32 Bassmaster Classic appearances and has fished Bassmaster events dating back to 1974.",
    torneos: 474,
    ganados: 16,
    foto: "https://www.bassmaster.com/sites/default/files/persons/media%2C%20browser/aj1i1748.jpg",
  },
  {
    id: 6,
    nombre: "Greg di Palma",
    descripcion: "I was born and raised in New Jersey in a town called Millville. Fishing has always been in my blood being I grew up on the banks of the Maurice River.",
    torneos: 74,
    ganados: 0,
    foto: "https://www.bassmaster.com/sites/default/files/persons/media%2C%20browser/dipalma_greg_1.jpg"
  },
  {
    id: 7,
    nombre: "Harvey Horne",
    descripcion: "The stars of bass fishing typically inspire teenagers to dream of following in their footsteps.Harvey Horne ",
    torneos: 50,
    ganados: 0,
    foto: "https://www.bassmaster.com/sites/default/files/persons/media%2C%20browser/horne_harvey_1.jpg"
  },
  {
    id: 8,
    nombre: "Clent Davis",
    descripcion: "Clent Davis is a professional angler on the FLW Tour. He was the 2012 FLW Tour Rookie of the Year.",
    torneos: 52,
    ganados: 0,
    foto: "https://www.bassmaster.com/sites/default/files/persons/media%2C%20browser/davis_clent_2.jpg"
  }
];


