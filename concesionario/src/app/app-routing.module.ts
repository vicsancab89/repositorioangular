import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetalleCocheComponent } from './detalle-coche/detalle-coche.component';
import { ListaCocheComponent } from './lista-coche/lista-coche.component';


const routes: Routes = [
  {
    path:'',
    component: ListaCocheComponent
  },
  {
    path: 'coche',
    component: ListaCocheComponent
  },
  {
    path: 'coche/:id',
    component: DetalleCocheComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
