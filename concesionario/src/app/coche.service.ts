import { Injectable } from '@angular/core';
import { COCHES } from './mocks/cocheMock';

@Injectable({
  providedIn: 'root'
})
export class CocheService {

  constructor() { }
  getCoches(){
    return COCHES;
  }
  getCoche(id: number){
    return COCHES[id];
  }

}
