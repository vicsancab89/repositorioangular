export interface Coche{
  id: number,
  modelo: string,
  precioBase: number,
  imagen: string,
  opciones: {
    acabado: string,
    motorizacion: string
  },
  texto: string
}
