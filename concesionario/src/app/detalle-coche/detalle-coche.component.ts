import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CocheService } from '../coche.service';

@Component({
  selector: 'app-detalle-coche',
  templateUrl: './detalle-coche.component.html',
  styleUrls: ['./detalle-coche.component.css']
})
export class DetalleCocheComponent implements OnInit {

  coche: any = {};
  formGroup !: FormGroup;
  constructor(private cocheService: CocheService, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder ) { }

  ngOnInit(): void {
    this.coche = this.cocheService.getCoche(this.activatedRoute.snapshot.params.id -1);
    this.buildForm();
  }
  private buildForm(){
    this.formGroup = this.formBuilder.group({
      motor: new FormControl(''),
      acabado: new FormControl(''),
      color: new FormControl('')
    });
  }
}
