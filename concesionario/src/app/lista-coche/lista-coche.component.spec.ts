import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaCocheComponent } from './lista-coche.component';

describe('ListaCocheComponent', () => {
  let component: ListaCocheComponent;
  let fixture: ComponentFixture<ListaCocheComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaCocheComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaCocheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
