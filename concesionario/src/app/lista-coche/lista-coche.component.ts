import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CocheService } from '../coche.service';

@Component({
  selector: 'app-lista-coche',
  templateUrl: './lista-coche.component.html',
  styleUrls: ['./lista-coche.component.css']
})
export class ListaCocheComponent implements OnInit {

  coches: any[] = [];
  constructor(private cocheService: CocheService, private router: Router) { }

  ngOnInit(): void {
    this.coches = this.cocheService.getCoches();
  }

}
