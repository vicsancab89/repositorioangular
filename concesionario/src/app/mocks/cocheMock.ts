import { Coche } from "../coche";

export const COCHES: Coche[] = [
  {
    id: 1,
    modelo: 'F30',
    precioBase: 12000,
    imagen: 'assets/F30.png',
    opciones: {
      acabado: 'base',
      motorizacion: 'gasolina'
    },
    precioFinal: 12000,
    texto: "Un utilitario para toda la familia, versátil, con su línea deportiva, clasificación energética A, respetuoso con el medio ambiente, y ¡con su bolsillo!"
  },
  {
    id: 2,
    modelo: 'F80',
    precioBase: 18000,
    imagen: 'assets/F80.png',
    opciones: {
      acabado: 'base',
      motorizacion: 'gasolina'
    },
    precioFinal: 18000,
    texto: 'Un coche para los amantes de la deportividad, cumple los máximos estándares en seguridad EURONCAP, respetuoso con el medio ambiente, en tres motorizaciones, ¡elige el tuyo!'
  },
  {
    id: 3,
    modelo: 'F100x',
    precioBase: 22000,
    imagen: 'assets/F100x.png',
    opciones: {
      acabado: 'base',
      motorizacion: 'gasolina'
    },
    precioFinal: 22000,
    texto: 'El SUV definitivo, clase A o 0 emisiones, amplitud, confort, hasta 7 plazas, ideal para la familia, para esas escapadas, todo un señor vehículo.'
  }
]
