import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Articulo } from 'src/app/shared/interfaces/articulo';


@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {
  @Input() cosa: any;
  @Output() enviar: EventEmitter<Articulo> = new EventEmitter<Articulo>();
  cambiado: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  enviarArticulo(){
    this.enviar.emit(this.cosa);
    this.cambiado = false;
  }
}
