import { Component, OnInit } from '@angular/core';
import { ARTICULOS } from 'src/app/shared/articulos';
import { Articulo } from 'src/app/shared/interfaces/articulo';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  public cosas: Articulo[] = [];
  public miCosa: any;
  constructor() {
    this.cosas = ARTICULOS;
   }

  ngOnInit(): void {
  }
  selectCosa(cosa: Articulo){
    this.miCosa = cosa;
  }
  recibir($event: Articulo){
   
  }

}
