import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlumnoComponent } from './componentes/alumno/alumno.component';
import { AlumnosComponent } from './componentes/alumnos/alumnos.component';
import { AutorComponent } from './componentes/autor/autor.component';
import { InicioComponent } from './componentes/inicio/inicio.component';

const routes: Routes = [
  {
    path: "",
    component: InicioComponent
  },
  {
    path: 'inicio',
    component: InicioComponent
  },
  {
    path: 'autor',
    component: AutorComponent
  },
  {
    path: 'alumnos',
    component: AlumnosComponent
  },
  {
    path: 'alumno/:id',
    component: AlumnoComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
