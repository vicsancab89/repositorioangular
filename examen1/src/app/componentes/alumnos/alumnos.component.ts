import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlumnosService } from 'src/app/servicios/alumnos.service';

@Component({
  selector: 'app-alumnos',
  templateUrl: './alumnos.component.html',
  styleUrls: ['./alumnos.component.css']
})
export class AlumnosComponent implements OnInit {

  alumnos: any[] = [];
  constructor( private alumnosService: AlumnosService, private router : Router) { }

  ngOnInit(): void {
    this.alumnos = this.alumnosService.getAlumnos();
  }

  showAlumno(id : number){
    this.router.navigate(['alumno', id]);
  }

}
