import { Component, OnInit } from '@angular/core';
import { ObjetivosService } from 'src/app/servicios/objetivos.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  objetivos: any[] = [];
  constructor(private objetivosService: ObjetivosService) { }

  ngOnInit(): void {
    this.objetivos = this.objetivosService.getObjetivos();
  }

}
