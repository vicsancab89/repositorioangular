export interface Alumnos {
  id: number,
  nombre: any[],
  curso: string,
  edad: number,
  activo: boolean,
}
