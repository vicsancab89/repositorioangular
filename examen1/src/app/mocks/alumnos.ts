export const ALUMNOS = [
  {
    id: 0,
    nombre: {
      apellidos: "Pére García",
      nom: "Alejandro"
    },
    curso: "CMI2",
    edad: 18,
    activo: true
  },
  {
    id: 1,
    nombre: {
      apellidos: "López Berenguer",
      nom: "Eduardo"
    },
    curso: "CMI2",
    edad: 17,
    activo: true
  },
  {
    id: 2,
    nombre: {
      apellidos: "Fernández Cortés",
      nom: "Carlos"
    },
    curso: "CSI1",
    activo: false
  },
  {
    id: 3,
    nombre: {
      apellidos: "García Garcés",
      nom: "Pepe"
    },
    curso: "CSI1",
    activo: true
  },
  {
    id: 4,
    nombre: {
      apellidos: "Lucero del Alba",
      nom: "Juan"
    },
    curso: "CSI2",
    activo: true
  },
  {
    id: 5,
    nombre: {
      apellidos: "Hierro Martín",
      nom: "Fernando"
    },
    curso: "CSI2",
    activo: true
  },
  {
    id: 6,
    nombre: {
      apellidos: "Luna Garzón",
      nom: "Antonio"
    },
    curso: "CSI2",
    activo: false
  }
];
