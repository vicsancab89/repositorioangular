export const OBJETIVOS = [
  {
    id: 1,
    txt: "Instalar los componentes"
  },
  {
    id: 2,
    txt: "Configurar los componentes: vista, controlador"
  },
  {
    id: 3,
    txt: "Creación de las rutas"
  },
  {
    id: 4,
    txt: "Insertar las rutas en la barra de navegación"
  },
  {
    id: 5,
    txt: "Creación de un servicio"
  },
  {
    id: 6,
    txt: "Programación del servicio que se pide"
  },
  {
    id: 7,
    txt: "Inyectar el servicio"
  },
  {
    id: 8,
    txt: "Uso del servicio inyectado en el componente"
  },
  {
    id: 9,
    txt: "Uso de directivas estructurales"
  },
  {
    id: 10,
    txt: "Aplicación de estilos con bootstrap o personalizados"
  }
]
