import { Injectable } from '@angular/core';
import { OBJETIVOS } from '../mocks/mockObjetivos';

@Injectable({
  providedIn: 'root'
})
export class ObjetivosService {

  constructor() { }
  getObjetivos(){
    return OBJETIVOS;
  }
}
