import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { LibroObservableService } from 'src/app/servicios/libro-observable.service';
import { Libro } from '../../libro.model';


@Component({
  selector: 'app-libro-lista',
  templateUrl: './libro-lista.component.html',
  styleUrls: ['./libro-lista.component.css']
})
export class LibroListaComponent implements OnInit {
  libros : Libro[] = [];
  observableSubs!: Subscription;

  constructor(private libroService: LibroObservableService) { }

  ngOnInit(): void {
    this.observableSubs = this.libroService.getLibros().subscribe(
      libros => this.libros = libros,
      error => console.error(error),
      () => console.log('Y ya estaaaaaaaaaaá')
    );
  }
  ngOnDestroy(): void{
    if (this.observableSubs) this.observableSubs.unsubscribe();
  }
}
