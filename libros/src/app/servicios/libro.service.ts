import { Injectable } from '@angular/core';
import { Libro } from '../libro.model';
import { LoggerService } from './logger.service';

@Injectable({
  providedIn: 'root'
})
export class LibroService {

  constructor(private loggerService: LoggerService) { }

  getLibros(): Promise<Libro[]>{
    return new Promise<Libro[]>((resolve, reject)=> {
    this.loggerService.log("Iniicio ejecutor (Promise de LibrosService.getLibros())");
    setTimeout()
    })
  }
}
