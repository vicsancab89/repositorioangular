import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PrincipalComponent } from './principal/principal.component';
import { HomeComponent } from './home/home.component';
import { MaterialModule } from './material.module';
import { JugadoresModule } from './gamer/jugadores.module';
import { JuegosModule } from './game/juegos.module';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    PrincipalComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    JugadoresModule,
    JuegosModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
