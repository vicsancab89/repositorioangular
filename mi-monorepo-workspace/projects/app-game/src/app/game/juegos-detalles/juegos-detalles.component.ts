import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { JUEGOS } from '../../mocks/juegos-mocks';

@Component({
  selector: 'app-juegos-detalles',
  templateUrl: './juegos-detalles.component.html',
  styleUrls: ['./juegos-detalles.component.css']
})
export class JuegosDetallesComponent implements OnInit {
  juego!: any | undefined
  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt (<string>params.get('id'));
      this.juego = JUEGOS.find(item => item.id === id);
    });
  }
}
