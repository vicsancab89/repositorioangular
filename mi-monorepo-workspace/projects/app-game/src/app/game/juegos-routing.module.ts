import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { JuegosDetallesComponent } from './juegos-detalles/juegos-detalles.component';
import { JuegosComponent } from './juegos/juegos.component';



const rutas: Routes = [
  {
    path: '',
    component: JuegosComponent
  },
  {
    path: ':id',
    component: JuegosDetallesComponent
  }

];
@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(rutas)
  ],
  exports: [
    RouterModule
  ]
})
export class JuegosRoutingModule{}

