import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JuegosComponent } from './juegos/juegos.component';
import { JuegosDetallesComponent } from './juegos-detalles/juegos-detalles.component';
import { JuegosRoutingModule } from './juegos-routing.module';



@NgModule({
  declarations: [
    JuegosComponent,
    JuegosDetallesComponent
  ],
  imports: [
    CommonModule,
    JuegosRoutingModule
  ]
})
export class JuegosModule { }
