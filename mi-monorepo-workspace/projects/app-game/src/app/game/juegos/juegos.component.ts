import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JUEGOS } from '../../mocks/juegos-mocks';

@Component({
  selector: 'app-juegos',
  templateUrl: './juegos.component.html',
  styleUrls: ['./juegos.component.css']
})
export class JuegosComponent implements OnInit {
  juegos: any[] = [];
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.juegos = JUEGOS.sort((a,b) => a.ranking - b.ranking);
  }
  muestraJuego(id: number){
    this.router.navigate(['juegos', id])
  }

}
