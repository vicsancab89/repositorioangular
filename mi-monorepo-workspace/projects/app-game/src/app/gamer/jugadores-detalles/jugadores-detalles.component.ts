import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { JUGADORES } from '../../mocks/jugadores-mocks';

@Component({
  selector: 'app-jugadores-detalles',
  templateUrl: './jugadores-detalles.component.html',
  styleUrls: ['./jugadores-detalles.component.css']
})
export class JugadoresDetallesComponent implements OnInit {
  jugador: any;
  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params: ParamMap)=>
    {
      let id = parseInt (<string>params.get('id'));
      this.jugador = JUGADORES.find(item => item.id === id);
    });
  }

}
