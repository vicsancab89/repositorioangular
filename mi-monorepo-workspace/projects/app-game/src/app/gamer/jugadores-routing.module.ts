import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { JugadoresComponent } from './jugadores/jugadores.component';
import { JugadoresDetallesComponent } from './jugadores-detalles/jugadores-detalles.component';


const rutas: Routes = [
  {
    path: '',
    component: JugadoresComponent
  },
  {
    path: ':id',
    component: JugadoresDetallesComponent
  }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(rutas)
  ],
  exports: [
    RouterModule
  ]
})
export class JugadoresRoutingModule { }
