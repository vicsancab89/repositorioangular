import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JugadoresComponent } from './jugadores/jugadores.component';
import { JugadoresDetallesComponent } from './jugadores-detalles/jugadores-detalles.component';
import { JugadoresRoutingModule } from './jugadores-routing.module';



@NgModule({
  declarations: [
    JugadoresComponent,
    JugadoresDetallesComponent
  ],
  imports: [
    CommonModule,
    JugadoresRoutingModule
  ],
})
export class JugadoresModule { }
