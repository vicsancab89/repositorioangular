import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JUGADORES } from '../../mocks/jugadores-mocks';

@Component({
  selector: 'app-jugadores',
  templateUrl: './jugadores.component.html',
  styleUrls: ['./jugadores.component.css']
})
export class JugadoresComponent implements OnInit {

  jugadores: any[] = [];
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.jugadores = JUGADORES.sort((a,b) => a.ranking - b.ranking);
  }
  muestraJugador(id: number){
    this.router.navigate(['jugadores', id])
  }

}
