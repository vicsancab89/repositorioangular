export class juegos
{
  id: number;
  nombre: string;
  editor: string;
  posicion: number;
  caratula: string;
  ranking: number;

  constructor(){
  this.id = 0;
  this.nombre = "";
  this.editor = "";
  this.posicion = 0;
  this.caratula = "";
  this.ranking = 0;
  }
}
