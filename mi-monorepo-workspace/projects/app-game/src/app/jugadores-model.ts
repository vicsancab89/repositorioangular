export class Jugadores{
  id: number;
  nombre: string;
  nick: string;
  avatar: string;
  ranking: number;
  constructor(){
    this.id = 0;
    this.nombre = "";
    this.nick = "";
    this.avatar = "";
    this.ranking = 0;
  }
}
