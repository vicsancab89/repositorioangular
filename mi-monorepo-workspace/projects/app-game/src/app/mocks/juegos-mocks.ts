import { juegos } from "../juegos-model";

export const JUEGOS : juegos[] = [
  {
    id: 1,
    nombre: "Metal Gear Solid",
    editor: "Konami",
    posicion: 1,
    caratula: "https://commondatastorage.googleapis.com/images.pricecharting.com/b585c1479447e3388f0f2d3d7fac705cb5b8f9a87c701187f465e0daef3ec74c/240.jpg",
    ranking: 1

  },
  { id: 2,
    nombre: "Gran Turismo II",
    editor: "Poliphony Digital",
    posicion: 2,
    caratula: "https://m.media-amazon.com/images/I/514GZ3W9EPL._AC_SX466_.jpg",
    ranking: 3
  },
  {
    id: 3,
    nombre: "Medievil",
    editor: "SCE",
    posicion: 3,
    caratula: "https://www.covercentury.com/covers/psx/m/Medievil-PAL-PSX-FRONT.jpg",
    ranking: 4
  },
  {
    id: 4,
    nombre: "Teken III",
    editor: "Namco",
    posicion: 4,
    caratula: "https://romsjuegos.com/wp-content/uploads/tekken-3-u-slus-playstation.jpg",
    ranking: 2
  }
];
