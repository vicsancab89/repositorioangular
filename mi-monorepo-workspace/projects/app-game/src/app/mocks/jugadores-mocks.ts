import { Jugadores } from "../jugadores-model";

export const JUGADORES : Jugadores[] = [
  {
    id: 1,
    nombre: "Tyler Beblins",
    nick: "Ninja",
    avatar: "https://sm.ign.com/ign_latam/screenshot/default/ninja-blevins-1_djea.jpg",
    ranking: 2
  },
  {
    id: 2,
    nombre: "Felix Kjellberg",
    nick: "PewDiePie",
    avatar: "https://fotos00.noticiasdenavarra.com/2020/05/05/690x278/pewdiepie.jpg",
    ranking: 3
  },
  {
    id: 3,
    nombre: "Preston",
    nick: "Preston Armament",
    avatar: "https://dmn-dallas-news-prod.cdn.arcpublishing.com/resizer/JJpmv4mbkTRBSA8O3hyXQ8U_58w=/1660x0/smart/filters:no_upscale()/arc-anglerfish-arc2-prod-dmn.s3.amazonaws.com/public/66FZSKGY7BJAWYT2735OLWSZDA.jpg",
    ranking: 1
  }
];
