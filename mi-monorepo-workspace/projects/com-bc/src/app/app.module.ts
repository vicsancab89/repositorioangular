import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { Component } from '@angular/core';
import { Compo1Component } from 'projects/app1/src/app/compo1/compo1.component';

@NgModule({
  declarations: [AppComponent, Compo1Component, Compo1Component],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
