import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';



@Component({
  selector: 'app-compo1',
  templateUrl: './compo1.component.html',
  styleUrls: ['./compo1.component.css']
})
export class Compo1Component implements OnInit {

  @Input() valor1!: string;
  @Input() valor2!: string;
  aux1!: number;
  aux2!: number;
  @Output()
  envRes: EventEmitter<number> = new EventEmitter<number>();

  constructor() { }

  ngAfterContentChecked(){
    this.aux1 = parseFloat(this.valor1)
    this.aux2 = parseFloat(this.valor2)
  }
  ngOnInit(): void {
  }

  suma() { this.envRes.emit(this.aux1 + this.aux2); }
  resta() { this.envRes.emit(this.aux1 - this.aux2); }
  multiplica() { this.envRes.emit(this.aux1 * this.aux2); }
  divide() { this.envRes.emit(this.aux1 / this.aux2); }

}
