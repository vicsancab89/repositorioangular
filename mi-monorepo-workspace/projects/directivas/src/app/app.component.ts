import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'directivas';
  fondoAmarillo = false;
  letraGrande = false;

  title1 = '024 dirNgIf';
  grupo1=false;
  grupo2=false;
  subgrupo11=false;
  subgrupo12=false;
  subgrupo21=false;
  subgrupo22=false;

  checkFondo(){
    this.fondoAmarillo = !this.fondoAmarillo;
  }
  checkLetra(){
    this.letraGrande = !this.letraGrande;
  }
  asignaClases(){
    let classes = {
      fa: this.fondoAmarillo,
      lg: this.letraGrande
    };
    return classes;
  }

}
