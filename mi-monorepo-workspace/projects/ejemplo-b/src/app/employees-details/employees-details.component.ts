import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmpleadosService } from '../servicios/empleados.service';

@Component({
  selector: 'app-employees-details',
  templateUrl: './employees-details.component.html',
  styleUrls: ['./employees-details.component.css']
})
export class EmployeesDetailsComponent implements OnInit {

  empleado: any = {}
  constructor( private employeesService: EmpleadosService, private actiRuter: ActivatedRoute) { }

  ngOnInit(): void {
    this.actiRuter.params.subscribe((params) =>{
      this.empleado = this.employeesService.getEmpleado(params['id']);
    });
  }

}
