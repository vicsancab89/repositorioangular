import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmpleadosService } from '../servicios/empleados.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  employees: any[] = [];
  constructor(private employeesService : EmpleadosService, private ruter: Router) { }

  ngOnInit(): void {
    this.employees = this.employeesService.getEmpleados();
  }

  showEmploye(id : number){
    this.ruter.navigate(['Employe', id]);
  }
  filtrarEmpleados(active?: boolean){
    if(active == null){
      this.employees = this.employeesService.getEmpleados();
    }
    else if(active == true){
      this.employees = this.employeesService.getEmpleadosFiltrado(active);
    }
    else{
      this.employees = this.employeesService.getEmpleadosFiltrado(active);
    }
  }

}


