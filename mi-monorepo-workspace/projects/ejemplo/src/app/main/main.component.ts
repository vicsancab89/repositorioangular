import { Component, OnInit } from '@angular/core';
import { Grupo } from '../grupo';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  grupo1 : Grupo = {
    nombre : "Grupo 1",
    equipos : ["Madrid", "Sevilla", "Barcelona", "Betis"]
  }
  constructor() { }

  ngOnInit(): void {
  }

}
