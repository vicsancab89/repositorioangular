import { Injectable } from '@angular/core';
import { Equipo } from './equipo';
import { GrupoA, GrupoB, GrupoC, GrupoD } from './mocks/mock-grupos';

@Injectable({
  providedIn: 'root'
})
export class GruposService {
  equipo: any[] = [];
  constructor() { }

  getGrupo(grupo : string): Equipo[]{
    switch(grupo){
      case 'A':
        this.equipo = GrupoA;
        break;
      case 'B':
        this.equipo = GrupoB;
        break;
      case 'C':
        this.equipo = GrupoC;
        break;
      case 'D':
        this.equipo = GrupoD;
        break;
    }
    return this.equipo;
  }
}
