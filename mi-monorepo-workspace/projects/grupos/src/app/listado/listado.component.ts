import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route } from '@angular/router';
import { GruposService } from '../grupos.service';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {

  listaEquipos: any[] = [];

  constructor(private grupoService: GruposService, private route : ActivatedRoute) { }

  ngOnInit(): void {
    this.listaEquipos = this.grupoService.getGrupo(this.route.snapshot.params.grupo);
  }

}
