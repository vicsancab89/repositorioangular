import { Equipo } from "../equipo";

export const GrupoA : Equipo[] = [
  {
    id: 1,
    nombre: 'MENTES MAESTRAS',
    curso: 'TSEAS-1'
  },
  {
    id: 2,
    nombre: 'bulgaros',
    curso: 'TSEAS-2'
  },
  {
    id: 3,
    nombre: 'CMI2',
    curso: 'CMI-2'
  },
  {
    id: 4,
    nombre: 'La once',
    curso: 'CMGA-1'
  }
];
export const GrupoB : Equipo[] = [
  {
    id: 1,
    nombre: 'LOS ESTUDIANTES',
    curso: 'TSEAS-1'
  },
  {
    id: 2,
    nombre: 'MANCHESTER PITY',
    curso: 'TSEAS-2'
  },
  {
    id: 3,
    nombre: 'RECREATIVO DE JUERGA',
    curso: 'CMI-2'
  },
  {
    id: 4,
    nombre: 'CSI-2',
    curso: 'CSI-2'
  }
];
export const GrupoC : Equipo[] = [
  {
    id: 1,
    nombre: 'SEVILLA',
    curso: 'TSEAS-1'
  },
  {
    id: 2,
    nombre: 'MALAGA',
    curso: 'TSEAS-2'
  },
  {
    id: 3,
    nombre: 'BADAJOZ',
    curso: 'CMI-2'
  },
  {
    id: 4,
    nombre: 'CADIZ',
    curso: 'CMGA-1'
  }
];
export const GrupoD : Equipo[] = [
  {
    id: 1,
    nombre: 'MALLORCA',
    curso: 'TSEAS-1'
  },
  {
    id: 2,
    nombre: 'VALENCIA',
    curso: 'TSEAS-2'
  },
  {
    id: 3,
    nombre: 'ALAVÉS',
    curso: 'CMI-2'
  },
  {
    id: 4,
    nombre: 'ELCHE',
    curso: 'CSI-2'
  }
];
