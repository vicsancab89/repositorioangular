import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { PptModule } from './ppt/ppt.module';
import { RoutingModule } from './routing.module';
import { MenuComponent } from './menu/menu.component';
import { TerModule } from './ter/ter.module';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    PptModule,
    RoutingModule,
    TerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
