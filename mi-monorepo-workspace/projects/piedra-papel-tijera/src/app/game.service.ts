import { Injectable } from '@angular/core';
import { Resultado } from './resultado';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  changePlayerTurn(square: any) {
    throw new Error('Method not implemented.');
  }
  isGameRunning!: boolean;
  activePlayer: any;

  constructor() { }

  getComputerChoice(): string {
    const choices = ['r', 'p', 's']; // Roca, Papel, Tijeras
    const randomChoice = Math.floor(Math.random() * 3);
    return choices[randomChoice];
  }

  game(userChoice: string): Resultado{
    const playUserComp = userChoice + this.getComputerChoice();
    console.log(`Jugada realizada: ${playUserComp}`);
    let playStatus: Resultado;
    switch (playUserComp){

      // Ganamos
      case 'rs':
      case 'sp':
      case 'pr':
        // win(userChoice, computerChoice);
        playStatus = {
          message: 'Ganas a la computadora',
          userAdd: 1,
          compAdd: 0,
        };
        break;

        // Gana la computadora
      case 'rp':
      case 'ps':
      case 'sr':
        // lose(userChoice, computerChoice);
        playStatus = {
          message: 'Gana la computadora',
          userAdd: 0,
          compAdd: 1,
        };
        break;

        // Empatamos
      case 'rr':
      case 'pp':
      case 'ss':
        // draw(userChoice, computerChoice);
        playStatus = {
          message: 'Habéis elegido la misma jugada y habéis empatado',
          userAdd: 0,
          compAdd: 0,
        };
        break;
    }
    return playStatus!;
  }
}

