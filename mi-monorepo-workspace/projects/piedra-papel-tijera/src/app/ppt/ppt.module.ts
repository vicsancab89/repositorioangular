import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RoutingModule } from '../routing.module';

import { PptComponent } from './ppt/ppt.component';


@NgModule({
  declarations: [
    PptComponent
  ],
  imports: [
    CommonModule,
    RoutingModule
  ],
  exports: [
    PptComponent
  ]
})
export class PptModule { }
