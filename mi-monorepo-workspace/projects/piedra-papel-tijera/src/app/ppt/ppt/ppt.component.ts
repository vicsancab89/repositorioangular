import { Component, OnInit } from '@angular/core';
import { GameService } from '../../game.service';

@Component({
  selector: 'app-ppt',
  templateUrl: './ppt.component.html',
  styleUrls: ['./ppt.component.css'],
})
export class PptComponent implements OnInit {
  title = 'juego1'
  result?: string;
  pointsUser = 0;
  pointsComp = 0;

  constructor(private playGame: GameService){

  }

  ngOnInit(): void {
    this.result = 'Esperando jugada...';
    console.log(this.pointsUser);
  }

  play(choice: string): void {
    const resultado = this.playGame.game(choice);
    this.result = resultado.message;
    this.pointsUser += resultado.userAdd;
    this.pointsComp += resultado.compAdd;
  }
}
