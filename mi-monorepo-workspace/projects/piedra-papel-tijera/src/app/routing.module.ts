import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from './menu/menu.component';
import { PptComponent } from './ppt/ppt/ppt.component';
import { TerComponent } from './ter/ter/ter.component';

const  rutas: Routes = [
  {
    path: '',
    component: MenuComponent
  },
  {
    path: 'ppt',
    component: PptComponent
  },
  {
    path: 'ter',
    component: TerComponent
  }
];


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(rutas)
  ],
  exports: [
    RouterModule
  ]
})
export class RoutingModule { }
