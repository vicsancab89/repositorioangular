import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TerComponent } from './ter/ter.component';
import { RoutingModule } from '../routing.module';
import { BoardComponent } from './board/board.component';
import { SquareComponent } from './square/square.component';
import { FormsModule } from '@angular/forms';
import { GameService } from './game.service';



@NgModule({
  declarations: [
    TerComponent,
    BoardComponent,
    SquareComponent
  ],
  imports: [
    CommonModule,
    RoutingModule,
    FormsModule
  ],
  providers: [
    GameService
  ]
})
export class TerModule { }
