import { Component, OnInit } from '@angular/core';
import { GameService } from '../game.service';

@Component({
  selector: 'app-ter',
  templateUrl: './ter.component.html',
  styleUrls: ['./ter.component.css']
})
export class TerComponent implements OnInit {

  constructor(public gameService: GameService) { }

  ngOnInit(): void {
  }

  resetGame(){
    this.gameService.newGame()
  }
}
