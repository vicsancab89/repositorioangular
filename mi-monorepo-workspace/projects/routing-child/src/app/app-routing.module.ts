import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SaludoComponent } from './saludo/saludo.component';

const routes: Routes = [
  {
    path: 'saludo',
    component: SaludoComponent
  },
  {
    path: 'libros',
    loadChildren: () => import('./libros/libros.module').then (m => m.LibrosModule)
  },

  {
    path: 'autores',
    loadChildren: () => import('./autores/autores.module').then (m => m.AutoresModule)
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/saludo'
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '/libros'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
