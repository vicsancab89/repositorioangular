import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AutorListaComponent } from './autor-lista/autor-lista.component';


const rutas: Routes = [
  {
    path: '',
    component: AutorListaComponent
  }
]
@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(rutas)
  ],
  exports: [
    RouterModule
  ]
})
export class AutoresRoutingModule { }
