import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutorListaComponent } from './autor-lista/autor-lista.component';
import { AutoresRoutingModule } from './autores-routing.module';



@NgModule({
  declarations: [
    AutorListaComponent
  ],
  imports: [
    CommonModule,
    AutoresRoutingModule
  ]
})
export class AutoresModule { }
