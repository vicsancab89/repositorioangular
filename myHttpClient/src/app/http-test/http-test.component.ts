import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-http-test',
  templateUrl: './http-test.component.html',
  styleUrls: ['./http-test.component.css']
})
export class HttpTestComponent implements OnInit {

  resultadoPeticion: any;
  constructor(private http:HttpClient) { }

  ngOnInit(): void {
  }
  get() {
    this.http.get('https://jsonplaceholder.typicode.com/posts')
    .subscribe( data => { this.resultadoPeticion = data; } );
    }
    post(){
    this.http.post('https://jsonplaceholder.typicode.com/posts',
    {
    title: 'Previsión Viernes.',
    body: 'Parcialmente soleado.',
    userId: 1
    })
    .subscribe( data => { this.resultadoPeticion = data; } );

  }
  get_param() {
    const params = new HttpParams().set('userId', '9');
    this.http.get('https://jsonplaceholder.typicode.com/posts',
   {params})
    .subscribe( data => { this.resultadoPeticion = data; } );
  }

  put(){
    this.http.put('https://jsonplaceholder.typicode.com/posts/1000',
    {
    id: 1,
    title: 'Previsión Lunes',
    body: 'Lluvias.',
    userId: 1
    })
    .subscribe(
      (data) => { this.resultadoPeticion = data; },
      (error) => { console.error(error); }
      );
    }
    patch(){
    this.http.patch('https://jsonplaceholder.typicode.com/posts/1',
    {
    body: 'Soleado.'
    })
    .subscribe( data => { this.resultadoPeticion = data; } );
    }
    delete(){
    this.http.delete('https://jsonplaceholder.typicode.com/posts/1')
    .subscribe( data => { this.resultadoPeticion = data; } );
    }

}
