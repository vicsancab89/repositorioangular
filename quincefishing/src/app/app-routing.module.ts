import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BaitComponent } from './bait/bait.component';
import { HomeComponent } from './home/home.component';
import { HookComponent } from './hook/hook.component';
import { LakeComponent } from './lake/lake.component';
import { LineComponent } from './line/line.component';
import { ReelComponent } from './reel/reel.component';
import { RodComponent } from './rod/rod.component';

const routes: Routes = [
  {
    path:'home',
    component: HomeComponent
  },
  {
    path: 'bait',
    component: BaitComponent
  },
  {
    path: 'bait/:cebos',
    component: BaitComponent
  },
  {
    path: 'hook',
    component: HookComponent
  },
  {
    path: 'hook/:anzuelos',
    component: HookComponent
  },
  {
    path:'lake',
    component: LakeComponent
  },
  {
    path:'lake/:spot',
    component: LakeComponent
  },
  {
    path: 'line',
    component: LineComponent
  },
  {
    path: 'line/:hilo',
    component: LineComponent
  },
  {
    path: 'reel',
    component: ReelComponent
  },
  {
    path: 'reel/:carretes',
    component: ReelComponent
  },
  {
    path: 'rod',
    component: RodComponent
  },
  {
    path: 'rod/:stick',
    component: RodComponent
  },
  {
    path: ' ',
    redirectTo: '/home',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
