import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LakeComponent } from './lake/lake.component';
import { RodComponent } from './rod/rod.component';
import { ReelComponent } from './reel/reel.component';
import { LineComponent } from './line/line.component';
import { HookComponent } from './hook/hook.component';
import { BaitComponent } from './bait/bait.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LakeComponent,
    RodComponent,
    ReelComponent,
    LineComponent,
    HookComponent,
    BaitComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
