export interface Lake {
  id: number;
  nombre: string;
  provincia: string;
  navegable: boolean;
  foto: string;
}
