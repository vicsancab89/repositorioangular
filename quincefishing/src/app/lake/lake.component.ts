import { Component, OnInit } from '@angular/core';
import { LakeService } from '../services/lake.service';

@Component({
  selector: 'app-lake',
  templateUrl: './lake.component.html',
  styleUrls: ['./lake.component.css']
})
export class LakeComponent implements OnInit {
  lakes: any[] = [];
  constructor(private lakesService : LakeService) { }

  ngOnInit(): void {
    this.lakes = this.lakesService.getLake();
  }

  selectLake(active?: boolean){
    if(active == null)
      this.lakes = this.lakesService.getLake();
    else if(active == true)
      this.lakes = this.lakesService.getSelectLake(active);
    else
      this.lakes = this.lakesService.getSelectLake(active);
  }

}
