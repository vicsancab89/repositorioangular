import { Lake } from "../interfaces/lake";

export const Lagos : Lake[] = [
  {
    id: 1,
    nombre: "La Minilla",
    provincia: "Sevilla",
    navegable: false,
    foto: "https://www.diariodesevilla.es/2019/05/23/sociedad/bellas-imagenes-pantano-Minilla_1357374667_100301127_667x375.jpg",
  },
  {
    id: 2,
    nombre: "La Breña",
    provincia: "Córdoba",
    navegable: true,
    foto: "https://sevilla.abc.es/media/MM/2019/09/30/s/pantano-brena-cordoba(2)_xoptimizadax.jpg",
  },
  {
    id: 3,
    nombre: "El Piedras",
    provincia: "Huelva",
    navegable: true,
    foto: "https://www.cotodepezca.com/wp-content/uploads/2015/07/embalse-piedras.jpg",
  }
];
