import { Injectable } from '@angular/core';
import { Lagos } from '../mocks/mock-lake';

@Injectable({
  providedIn: 'root'
})
export class LakeService {

  lake: any[] = Lagos;

  constructor() { }

  getLake(){
    return this.lake;
  }
  getSelectLake(active: boolean){
    return this.lake.filter(lago => lago.navegable == active)
  }
}
